import React, {Component} from 'react';
import Wrapper from '../../hoc/Wrapper';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';

const INGREDIENTS_PRICES = {
  salad: 0.5,
  cheese: 0.3,
  meat: 1.5,
  bacon: 1,
};

class BurgerBuilder extends Component {
  state = {
    ingredients: {
      salad: 0,
      bacon: 0,
      cheese: 0,
      meat: 0,
    },
    totalPrice: 6,
    purchasable: false,
    checkout: false,
  };

  readyToBuy = ingredients => {
    let count = 0;
    for (let key in ingredients) {
      count += ingredients[key];
    }

    /*
    if (count <= 0) {
      this.setState({purchasable: false});
    } else {
      this.setState({purchasable: true});
    }
    */

    this.setState({purchasable: count > 0});
  };

  addIngredientHandler = type => {
    const oldCount = this.state.ingredients[type];
    const updatedCount = oldCount + 1;

    const updatedIngredients = {
      ...this.state.ingredients,
    };
    updatedIngredients[type] = updatedCount;

    const additionalPrice = INGREDIENTS_PRICES[type];
    const oldPrice = this.state.totalPrice;
    const newPrice = oldPrice + additionalPrice;

    this.setState({totalPrice: newPrice, ingredients: updatedIngredients});

    this.readyToBuy(updatedIngredients);
  };

  removeIngredientHandler = type => {
    const oldCount = this.state.ingredients[type];
    if (oldCount <= 0) return;
    const updatedCount = oldCount - 1;

    const updatedIngredients = {
      ...this.state.ingredients,
    };
    updatedIngredients[type] = updatedCount;

    const reducedPrice = INGREDIENTS_PRICES[type];
    const oldPrice = this.state.totalPrice;
    const newPrice = oldPrice - reducedPrice;

    this.setState({totalPrice: newPrice, ingredients: updatedIngredients});
    this.readyToBuy(updatedIngredients);
  };

  checkoutHandler = () => {
    this.setState({checkout: true});
  };

  render = () => {
    const disabledInfo = {
      ...this.state.ingredients,
    };

    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }
    console.log(disabledInfo);

    return (
      <Wrapper>
        <Modal show={this.state.checkout}>
          <OrderSummary ingredients={this.state.ingredients} />
        </Modal>
        <Burger ingredients={this.state.ingredients} />
        <BuildControls
          addIngredient={this.addIngredientHandler}
          removeIngredient={this.removeIngredientHandler}
          disabled={disabledInfo}
          price={this.state.totalPrice}
          purchasable={this.state.purchasable}
          checkout={this.checkoutHandler}
        />
      </Wrapper>
    );
  };
}

export default BurgerBuilder;
