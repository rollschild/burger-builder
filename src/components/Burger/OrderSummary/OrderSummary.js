import React from 'react';
import Wrapper from '../../../hoc/Wrapper';

const orderSummary = props => {
  const ingredientsList = Object.keys(props.ingredients).map(ingKey => {
    return (
      <li key={ingKey}>
        <span style={{textTransform: 'capitalize'}}>{ingKey}:</span>{' '}
        {props.ingredients[ingKey]}
      </li>
    );
  });
  return (
    <Wrapper>
      <h2>Your Order Summary:</h2>
      <p>List of Ingredients:</p>
      <ul>{ingredientsList}</ul>
      <p>Go ahead and checkout?</p>
    </Wrapper>
  );
};

export default orderSummary;
