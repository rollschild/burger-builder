import React from 'react';
import styleClasses from './BuildControl.css';

const buildControl = props => (
  <div className={styleClasses.BuildControl}>
    <div className={styleClasses.Label}>{props.label}</div>
    <button className={styleClasses.Add} onClick={props.add}>
      Add
    </button>
    <button
      className={styleClasses.Remove}
      onClick={props.remove}
      disabled={props.disabled}>
      Remove
    </button>
  </div>
);

export default buildControl;
