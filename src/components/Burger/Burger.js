import React from 'react';
import styleClasses from './Burger.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const burger = props => {
  let ingredientsList = Object.keys(props.ingredients)
    .map(ingProp => {
      return [...Array(props.ingredients[ingProp])].map((_, index) => {
        return <BurgerIngredient key={ingProp + index} type={ingProp} />;
      }); // an array of arrays
    })
    .reduce((arr, el) => {
      return arr.concat(el);
    }, []); // one flattened array

  if (ingredientsList.length === 0) {
    ingredientsList = <p>Please start adding ingredients...</p>;
  }

  /*
   * You NEED a *key* prop inside the BurgerIngredient
   * component, because the list returns an array of
   * components and the *key* is required.
   */

  return (
    <div className={styleClasses.Burger}>
      <BurgerIngredient type="bread-top" />
      {ingredientsList}
      <BurgerIngredient type="bread-bottom" />
    </div>
  );
};

export default burger;
