import React from 'react';
import Wrapper from '../../hoc/Wrapper';
import styleClasses from './Layout.css';

const layout = props => (
  <Wrapper>
    <div>Toolbar, SideDrawer, Backdrop</div>
    <main className={styleClasses.Content}>{props.children}</main>
  </Wrapper>
); // function

export default layout;
